var appUtil = require('./data/apputility/app');
//Setting up server
var server = appUtil.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});

