var express = require('express');
var pdf = require('../puppteer/scrape');
var router = express.Router();
router.route('/getPDFbyURL').post(pdf.run);
module.exports = router;