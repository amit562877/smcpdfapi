const puppeteer = require('puppeteer');
const hummus = require('hummus');
const memoryStreams = require('memory-streams');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var stream = require('stream');
const fs = require("fs");
const combinePDFBuffers = (firstBuffer, secondBuffer) => {
    var outStream = new memoryStreams.WritableStream();
    try {
        var firstPDFStream = new hummus.PDFRStreamForBuffer(firstBuffer);
        var secondPDFStream = new hummus.PDFRStreamForBuffer(secondBuffer);

        var pdfWriter = hummus.createWriterToModify(firstPDFStream, new hummus.PDFStreamForResponse(outStream));
        pdfWriter.appendPDFPagesFromPDF(secondPDFStream);
        pdfWriter.end();
        var newBuffer = outStream.toBuffer();
        outStream.end();
        return newBuffer;
    }
    catch (e) {
        outStream.end();
        throw new Error('Error during PDF combination: ' + e.message);
    }
};

async function run(req, res) {
    const cache = {};
    let browser = null, page = null;
    try {
        (async function () {
            try {

                try {
                    browser = await puppeteer.launch({
                        pipe: true,
                        headless: true,
                        args: [
                            '--no-sandbox', '--disable-setuid-sandbox',
                            '--disk-cache-dir=./Temp/browser-cache-disk',
                        ],
                    });
                    page = await browser.newPage();
                    await page._client.send('Network.setCacheDisabled', {
                        cacheDisabled: false
                    });
                    try {
                        // const response = await page.goto(req.body.url, {
                        //     waitFor: { timeout: 0, 'waitUntil': 'networkidle0' },
                        // })
                        await page.setContent(req.body.content);
                        let pdfData;
                        await page.emulateMediaType(req.body.emulateMedia);
                        pdfData = await page.pdf({
                            format: req.body.format,
                            height: req.body.height,
                            margin: req.body.margin,
                            displayHeaderFooter: req.body.displayHeaderFooter,
                            headerTemplate: req.body.headerTemplate,
                            footerTemplate: req.body.footerTemplate,
                            printBackground: req.body.printBackground,
                        }).then(data => {
                            return data;
                        });
                        if (req.body.sendmail) {
                            var transporter = nodemailer.createTransport({
                                host: "gmail.smtp.com",
                                service: "gmail",
                                port: 587,
                                secure: false,
                                auth: {
                                    user: 'noreply1.lawsuit@gmail.com',
                                    pass: 'LawSuit@Levons@32@'
                                },
                                tls: {
                                    rejectUnauthorized: false
                                }
                            });
                            var mailOptions = {
                                from: '"LawSuit - The Unique Case Finder" <noreply1.lawsuit@gmail.com>',
                                to: req.body.mailTo.emailid,
                                bcc: 'akshar@levons.in',
                                subject: req.body.mailSubject,
                                text: (req.body.mailBody) ? req.body.mailBody : '',
                                html: (req.body.mailTemplate) ? req.body.mailTemplate : '',
                                attachments: [{
                                    filename: (req.body.fileName) ? req.body.fileName : 'details.pdf',
                                    content: new Buffer(pdfData, 'binary'),
                                    contentType: 'application/pdf'
                                }]
                            };
                            await browser.close();
                            transporter.sendMail(mailOptions, function (error, info) {
                                if (error) {
                                    res.status(200).send({ error: error });
                                } else {
                                    res.status(200).send({ mailresponse: info.response, flag: true, message: 'PDF has been sent to you email, please check inbox for the PDF' });
                                }
                            });
                        } else {
                            await browser.close();
                            res.setHeader('Content-Disposition', 'attachment; filename=panda.pdf');
                            res.setHeader('Content-Transfer-Encoding', 'binary');
                            res.setHeader('Content-Type', 'application/octet-stream');
                            var fileContents = Buffer.from(pdfData, "base64");
                            var readStream = new stream.PassThrough();
                            readStream.end(fileContents);
                            readStream.pipe(res);
                        }
                    } catch (err) {
                        await browser.close();
                        res.status(400).send({ error: err.toString() });
                    }
                } catch (err) {
                    await browser.close();
                    res.status(400).send({ error: err.toString() });
                }
                await browser.close();
            } catch (err) {
                await browser.close();
                res.status(200).send({ error: err.toString() });
            }
        })();
    } catch (err) {
        await browser.close();
        res.status(200).send({ error: err.toString() });
    }
}
function bufferToStream(buffer) {
    let stream = new Duplex();
    stream.push(buffer);
    stream.push(null);
    return stream;
}

module.exports = {
    run
};