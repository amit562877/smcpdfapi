async function loadModule(universityid) {
    var path = '';

    switch (universityid) {
        case 'australiancatholicuniversity': {
            path = './SrappingFiles/Australia/Government/australiancatholicuniversity/ScrapeCourse';
            break;
        }
        case 'australiannationaluniversity': {
            path = './SrappingFiles/Australia/Government/australiannationaluniversity/ScrapeCourse';
            break;
        }
        case 'bonduniversity': {
            path = './SrappingFiles/Australia/Government/bonduniversity/ScrapeCourse';
            break;
        }
        case 'universityofcanberra': {
            path = './SrappingFiles/Australia/Government/canberrauniversity/ScrapeCourse';
            break;
        }
        case 'carnegiemellonuniversity': {
            path = './SrappingFiles/Australia/Government/carnegiemellonuniversity/ScrapeCourse';
            break;
        }
        case 'charlesdarwinuniversity': {
            path = './SrappingFiles/Australia/Government/charlesdarwinuniversity/ScrapeCourse';
            break;
        }
        case 'charlessturtuniversity': {
            path = './SrappingFiles/Australia/Government/charlessturtuniversity/ScrapeCourse';
            break;
        }
        case 'cquniversity': {
            path = './SrappingFiles/Australia/Government/cquniversity/ScrapeCourse';
            break;
        }
        case 'curtinuniversity': {
            path = './SrappingFiles/Australia/Government/curtinuniversity/ScrapeCourse';
            break;
        }
        case 'deakinuniversity': {
            path = './SrappingFiles/Australia/Government/deakinuniversity/ScrapeCourse';
            break;
        }
        case 'edithcowanuniversity': {
            path = './SrappingFiles/Australia/Government/edithcowanuniversity/ScrapeCourse';
            break;
        }
        case 'federationuniversity': {
            path = './SrappingFiles/Australia/Government/federationuniversity/ScrapeCourse';
            break;
        }
        case 'flindersuniversity': {
            path = './SrappingFiles/Australia/Government/flindersuniversity/ScrapeCourse';
            break;
        }
        case 'griffithuniversity': {
            path = './SrappingFiles/Australia/Government/griffithuniversity/ScrapeCourse';
            break;
        }
        case 'jamescookuniversity': {
            path = './SrappingFiles/Australia/Government/jamescookuniversity/ScrapeCourse';
            break;
        }
        case 'latrobeuniversity': {
            path = './SrappingFiles/Australia/Government/latrobeuniversity/ScrapeCourse';
            break;
        }
        case 'macquarieuniversity': {
            path = './SrappingFiles/Australia/Government/macquarieuniversity/ScrapeCourse';
            break;
        }
        case 'monashuniversity': {
            path = './SrappingFiles/Australia/Government/monashuniversity/ScrapeCourse';
            break;
        }
        case 'murdochuniversity': {
            path = './SrappingFiles/Australia/Government/murdochuniversity/ScrapeCourse';
            break;
        }
        case 'queenslanduniversityoftechnology': {
            path = './SrappingFiles/Australia/Government/queenslanduniversityoftechnology/ScrapeCourse';
            break;
        }
        case 'rmituniversity': {
            path = './SrappingFiles/Australia/Government/rmituniversity/ScrapeCourse';
            break;
        }
        case 'southerncrossuniversity': {
            path = './SrappingFiles/Australia/Government/southerncrossuniversity/ScrapeCourse';
            break;
        }
        case 'swinburneuniversityoftechnology': {
            path = './SrappingFiles/Australia/Government/swinburneuniversityoftechnology/ScrapeCourse';
            break;
        }
        case 'theuniversityofadelaide': {
            path = './SrappingFiles/Australia/Government/theuniversityofadelaide/ScrapeCourse';
            break;
        }
        case 'theuniversityofmelbourne': {
            path = './SrappingFiles/Australia/Government/theuniversityofmelbourne/ScrapeCourse';
            break;
        }
        case 'theuniversityofnewcastle': {
            path = './SrappingFiles/Australia/Government/theuniversityofnewcastle/ScrapeCourse';
            break;
        }
        case 'theuniversityofnewcastle_phd': {
            path = './SrappingFiles/Australia/Government/theuniversityofnewcastle_master_doctor/ScrapeCourse';
            break;
        }
        case 'theuniversityofnotredameaustralia': {
            path = './SrappingFiles/Australia/Government/theuniversityofnotredameaustralia/ScrapeCourse';
            break;
        }
        case 'theuniversityofqueensland': {
            path = './SrappingFiles/Australia/Government/theuniversityofqueensland/ScrapeCourse';
            break;
        }
        case 'theuniversityofsydney': {
            path = './SrappingFiles/Australia/Government/theuniversityofsydney/ScrapeCourse';
            break;
        }
        case 'theuniversityofwesternaustralia': {
            path = './SrappingFiles/Australia/Government/theuniversityofwesternaustralia/ScrapeCourse';
            break;
        }
        case 'torrensuniversity': {
            path = './SrappingFiles/Australia/Government/torrensuniversity/ScrapeCourse';
            break;
        }
        case 'universityofnewengland': {
            path = './SrappingFiles/Australia/Government/universityofnewengland/ScrapeCourse';
            break;
        }
        case 'universityofnewsouthwales': {
            path = './SrappingFiles/Australia/Government/universityofnewsouthwales/ScrapeCourse';
            break;
        }
        case 'universityofsouthaustralia': {
            path = './SrappingFiles/Australia/Government/universityofsouthaustralia/ScrapeCourse';
            break;
        }
        case 'universityofsouthernqueensland': {
            path = './SrappingFiles/Australia/Government/universityofsouthernqueensland/ScrapeCourse';
            break;
        }
        case 'universityoftasmania': {
            path = './SrappingFiles/Australia/Government/universityoftasmania/ScrapeCourse';
            break;
        }
        case 'universityoftechnologysydney': {
            path = './SrappingFiles/Australia/Government/universityoftechnologysydney/ScrapeCourse';
            break;
        }
        case 'universityofthesunshinecoast': {
            path = './SrappingFiles/Australia/Government/universityofthesunshinecoast/ScrapeCourse';
            break;
        }
        case 'universityofwollongong': {
            path = './SrappingFiles/Australia/Government/universityofwollongong/ScrapeCourse';
            break;
        }
        case 'victoriauniversity': {
            path = './SrappingFiles/Australia/Government/victoriauniversity/ScrapeCourse';
            break;
        }
        case 'westernsydneyuniversity': {
            path = './SrappingFiles/Australia/Government/westernsydneyuniversity/ScrapeCourse';
            break;
        }
        //Private university
        case 'apic': {
            path = './SrappingFiles/Australia/Private/apic/ScrapeCourse';
            break;
        }
        case 'boxhillinstitute': {
            path = './SrappingFiles/Australia/Private/boxhillinstitute/ScrapeCourse'
            break;
        }
        case 'canberrainsituteoftechnology': {
            path = './SrappingFiles/Australia/Private/canberrainsituteoftechnology/ScrapeCourse'
            break;
        }
        case 'chisholminstitute': {
            path = './SrappingFiles/Australia/Private/chisholminstitute/ScrapeCourse'
            break;
        }
        case 'engineeringinstituteoftechnology': {
            path = './SrappingFiles/Australia/Private/eit/ScrapeCourse'
            break;
        }
        case 'holmesgleninstitute': {
            path = './SrappingFiles/Australia/Private/holmesgleninstitute/ScrapeCourse'
            break;
        }
        case 'kanganinstitute': {
            path = './SrappingFiles/Australia/Private/kanganinstitute/ScrapeCourse'
            break;
        }
        case 'melbourneinstituteoftechnology': {
            path = './SrappingFiles/Australia/Private/melbourneinstitueoftechnology/ScrapeCourse'
            break;
        }
        case 'tafeinternationalwesternaustralia': {
            path = './SrappingFiles/Australia/Private/tafeinternationalwesternaustralia/ScrapeCourse'
            break;
        }
        case 'tafequeensland': {
            path = './SrappingFiles/Australia/Private/tafequeensland/ScrapeCourse'
            break;
        }
        case 'tafesa': {
            path = './SrappingFiles/Australia/Private/tafesa/ScrapeCourse'
            break;
        }
        case 'thegordon': {
            path = './SrappingFiles/Australia/Private/thegordonuniversity/ScrapeCourse'
            break;
        }
        case 'williamanglissinstitute': {
            path = './SrappingFiles/Australia/Private/williamanglissinstitute/ScrapeCourse'
            break;
        }
        case 'acumen': {
            path = './SrappingFiles/Australia/Private/acumen/ScrapeCourse'
            break;
        }
        case 'acte': {
            path = './SrappingFiles/Australia/Private/acte/ScrapeCourse'
            break;
        }
        case 'aibt': {
            path = './SrappingFiles/Australia/Private/aibt/ScrapeCourse'
            break;
        }
        case 'alteccollege': {
            path = './SrappingFiles/Australia/Private/alteccollege/ScrapeCourse'
            break;
        }
        case 'ashtoncollege': {
            path = './SrappingFiles/Australia/Private/ashtoncollege/ScrapeCourse'
            break;
        }
        case 'brightoninstituteoftechnology': {
            path = './SrappingFiles/Australia/Private/brightoninstituteoftechnology/ScrapeCourse'
            break;
        }
        case 'danfordcollege': {
            path = './SrappingFiles/Australia/Private/danfordcollege/ScrapeCourse'
            break;
        }
        case 'dellainternational': {
            path = './SrappingFiles/Australia/Private/dellainternational/ScrapeCourse'
            break;
        }
        case 'educationaccessaustralia': {
            path = './SrappingFiles/Australia/Private/educationaccessaustralia/ScrapeCourse'
            break;
        }
        case 'everestinstitute': {
            path = './SrappingFiles/Australia/Private/everestinstitute/ScrapeCourse'
            break;
        }
        case 'ihna': {
            path = './SrappingFiles/Australia/Private/ihna/ScrapeCourse'
            break;
        }
        case 'imperialcollege': {
            path = './SrappingFiles/Australia/Private/imperialcollege/ScrapeCourse'
            break;
        }
        case 'jpinternationalcollege': {
            path = './SrappingFiles/Australia/Private/jpinternationalcollege/ScrapeCourse'
            break;
        }
        case 'jobtraininginstitute': {
            path = './SrappingFiles/Australia/Private/jobtraininginstitute/ScrapeCourse'
            break;
        }
        case 'kaplan': {
            path = './SrappingFiles/Australia/Private/kaplan/ScrapeCourse'
            break;
        }
        case 'kentinstitute': {
            path = './SrappingFiles/Australia/Private/kentinstitute/ScrapeCourse'
            break;
        }
        case 'lennoxcollege': {
            path = './SrappingFiles/Australia/Private/lennoxcollege/ScrapeCourse'
            break;
        }
        case 'melbournepolytechnic': {
            path = './SrappingFiles/Australia/Private/melbournepolytechnic/ScrapeCourse'
            break;
        }
        case 'menziesinstituteoftechnology': {
            path = './SrappingFiles/Australia/Private/menziesinstituteoftechnology/ScrapeCourse'
            break;
        }
        case 'novainstitute': {
            path = './SrappingFiles/Australia/Private/novainstitute/ScrapeCourse'
            break;
        }
        case 'orange': {
            path = './SrappingFiles/Australia/Private/orange/ScrapeCourse'
            break;
        }
        case 'rgit': {
            path = './SrappingFiles/Australia/Private/rgit/ScrapeCourse'
            break;
        }
        case 'rhodes': {
            path = './SrappingFiles/Australia/Private/rhodes/ScrapeCourse'
            break;
        }
        case 'shic': {
            path = './SrappingFiles/Australia/Private/shic/ScrapeCourse'
            break;
        }
        case 'sheridancollege': {
            path = './SrappingFiles/Australia/Private/sheridancollege/ScrapeCourse'
            break;
        }
        case 'stpetersinstitute': {
            path = './SrappingFiles/Australia/Private/stpetersinstitute/ScrapeCourse'
            break;
        }
        case 'sunshinecollegeofmanagement': {
            path = './SrappingFiles/Australia/Private/sunshinecollegeofmanagement/ScrapeCourse'
            break;
        }
        case 'thinkeducation': {
            path = './SrappingFiles/Australia/Private/thinkeducation/ScrapeCourse'
            break;
        }
        case 'victorianinstituteoftechnology': {
            path = './SrappingFiles/Australia/Private/victorianinstituteoftechnology/ScrapeCourse'
            break;
        }
    }
    return path;
}
module.exports = { loadModule }