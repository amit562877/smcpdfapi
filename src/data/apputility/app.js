var express = require("express");
var compression = require('compression')
var bodyParser = require("body-parser");
var routes = require('../routes/routes');
var cors = require('cors')
var path = require('path')

var app = express();

app.use(cors());
//CORS Middleware
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});
app.use(bodyParser({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(compression())

app.use(express.static(__dirname + '../'));
// app.use('/scrapeme/api', routes); //if you have added your application/api as application under website
app.use('/api', routes); //if you have added your application/api we webste



module.exports = app;